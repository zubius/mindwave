﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace MindWave
{
    class GraphPanel : Panel
    {
        private const int WideCount = 100;
        private List<float> _Y = new List<float>();
        public bool flag;

        public GraphPanel()
        {
            this.DoubleBuffered = true;
            this.ResizeRedraw = true;
        }

        public void AddPoints(float Y)
        {
            _Y.Add(Y);

            while (_Y.Count > WideCount)
                _Y.RemoveAt(0);

            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (_Y.Count > 1)
            {
                Point[] pnt = new Point[_Y.Count];

                int WidthKoef = (int)(this.Width / WideCount);
                int HeightKoef = (int)(this.Height / 100);

                for (short i = 1; i <= _Y.Count; i++)
                {
                    int yCoord = Math.Abs((Convert.ToInt32(_Y[i - 1]) * HeightKoef) - this.Height);
                    Point TmpP = new Point(WidthKoef * i, yCoord);
                    pnt[i - 1] = TmpP;
                }

                Pen P = new Pen(Color.Red, 2);

                e.Graphics.DrawLines(P, pnt);

                flag = true;
            }
        }
    }
}
