﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MindWave
{
    public class Person
    {
        private StringBuilder data_sb;
        private StringBuilder pers_sb = new StringBuilder();
        private string _fullName = "";
        private string _age = "";
        private string _grade = "";
        private string _type = "";
        private string _testtime = "";
        private List<int> audio = new List<int>();
        private List<int> video = new List<int>();
        private List<int> kinect = new List<int>();
        private string _filename = "";
        private string _path = "";
        private List<string> loadedata = new List<string>();
        private int ch = -1;
        private bool _isAudio = false;
        private bool _isVideo = false;
        private bool _isKinect = false;

        private static Logger log = LogManager.GetCurrentClassLogger();

        public Person()
        {

        }

        public void StartStream(int channel)
        {
            data_sb = new StringBuilder(channel + " ");
            this.ch = channel;
        }

        public void StoreData(float data)
        {
            switch (this.ch)
            {
                case 0:
                    audio.Add((int)data);
                    break;
                case 1:
                    video.Add((int)data);
                    break;
                case 2:
                    kinect.Add((int)data);
                    break;
            }

            data_sb.Append(data);
            data_sb.Append(";");
        }

        public void SaveData()
        {
            switch (this.ch)
            {
                case 0:
                    _isAudio = true;
                    break;
                case 1:
                    _isVideo = true;
                    break;
                case 2:
                    _isKinect = true;
                    break;
            }

            IOdata.Save(_filename, data_sb.AppendLine());
        }

        public void SavePerson()
        {
            _testtime = DateTime.Now.ToString("ddMMyyyy HHmmss");

            pers_sb.AppendLine(_fullName);
            pers_sb.AppendLine(_age);
            pers_sb.AppendLine(_grade);
            pers_sb.AppendLine(_type);

            _filename = _grade + "-" + _fullName + "-" + _testtime + ".tst";
            IOdata.Save(_filename, pers_sb);
        }

        public int Load(string path, string file, string ext)
        {
            _filename = file + ext;
            _path = path;
            loadedata = IOdata.LoadData(path + _filename);
            _fullName = loadedata[0];
            _age = loadedata[1];
            _grade = loadedata[2];
            _type = loadedata[3];
            string[] parts = file.Split('-');
            _testtime = DateTime.ParseExact(parts[2], "ddMMyyyy HHmmss",
                System.Globalization.CultureInfo.InvariantCulture).ToString();

            if (loadedata.Count > 4)
            {
                for (int i = 1; i < loadedata.Count - 3; i++)
                {
                    string tmp = loadedata[3 + i];

                    if (!String.IsNullOrEmpty(tmp))//!Bad Move   Fix bug source
                    {

                    switch (tmp.ToCharArray()[0])
                    {
                        case '0':   //last item in new line symbol, remove it
                            //audio = Array.ConvertAll(tmp.Remove(0, 2).Remove(tmp.Length - 3).Split(';'), int.Parse);
                            audio = tmp.Remove(0, 2).Remove(tmp.Length - 3).Split(';').Select(n => int.Parse(n)).ToList();
                            _isAudio = true;
                            break;
                        case '1':
                            video = tmp.Remove(0, 2).Remove(tmp.Length - 3).Split(';').Select(n => int.Parse(n)).ToList();
                            _isVideo = true;
                            break;
                        case '2':
                            kinect = tmp.Remove(0, 2).Remove(tmp.Length - 3).Split(';').Select(n => int.Parse(n)).ToList();
                            _isKinect = true;
                            break;
                        default:
                            break;
                    }

                    }
                }
            }

            return -1;
        }

        public string FullName
        {
            get { return this._fullName; }
            set { this._fullName = value; }
        }

        public string Age
        {
            get { return this._age; }
            set { this._age = value; }
        }

        public string Grade
        {
            get { return this._grade; }
            set { this._grade = value; }
        }

        public string Type
        {
            get { return this._type; }
            set { this._type = value; }
        }

        public string Test
        {
            get { return this._testtime; }
        }

        public bool IsAudio
        {
            get { return this._isAudio; }
        }

        public bool IsVideo
        {
            get { return this._isVideo; }
        }

        public bool IsKinect
        {
            get { return this._isKinect; }
        }

        public List<int> Audio
        {
            get { return this.audio; }
        }

        public List<int> Video
        {
            get { return this.video; }
        }

        public List<int> Kinect
        {
            get { return this.kinect; }
        }

        public void Clear()
        {
            data_sb = new StringBuilder();
            pers_sb = new StringBuilder();
            _fullName = "";
            _age = "";
            _grade = "";
            _type = "";
            _testtime = "";
            audio = new List<int>();
            video = new List<int>();
            kinect = new List<int>();
            _filename = "";
            _path = "";
            loadedata = new List<string>();
            ch = -1;
            _isAudio = false;
            _isVideo = false;
            _isKinect = false;
        }

        internal void ChangeType(string type)
        {
            loadedata[3] = type;
            StringBuilder sb = new StringBuilder();

            foreach (string str in loadedata)
            {
                sb.AppendLine(str);
            }

            IOdata.Edit(_filename, sb);
        }
    }
}
