﻿using NLog;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace MindWave
{
    public partial class GraphForm : Form
    {
        GraphPanel gp;
        int con; //TGCD connectionID
        Thread th1;
        string form_text;
        private int ch; //channel of type data
        private string port = "";
        Person p;

        private static Logger log = LogManager.GetCurrentClassLogger();

        /*Channel of data
         * 0 - audio
         * 1 - video
         * 2 - kinect
         */
        public GraphForm(string form_text, int channel, Person p)
        {
            InitializeComponent();
            this.form_text = form_text;
            this.ch = channel;

            CreatePanel();
            this.p = p;
            this.p.StartStream(this.ch);

            string[] ports = System.IO.Ports.SerialPort.GetPortNames();
            comPort_cb.Items.AddRange(ports);
            comPort_cb.SelectedIndex = ports.Length - 1;
            port = comPort_cb.Text;

            try
            {
                th1 = new Thread(() => mw());
                th1.Name = "MW";
            }
            catch (Exception ex)
            {

            }
        }

        private void CreatePanel()
        {
            gp = new GraphPanel();
            gp.Parent = this;
            gp.Top = 10;
            gp.Left = 12;
            gp.Width = this.ClientRectangle.Width - 110;
            gp.Height = this.ClientRectangle.Height - 20;
            gp.BackColor = Color.White;
            gp.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top);
        }

        void mw()
        {
            try
            {
                con = ThinkGear.TG_GetNewConnectionId();
                int status = ThinkGear.TG_Connect(con, "\\\\.\\" + port, ThinkGear.BAUD_115200, ThinkGear.STREAM_PACKETS);
            }
            catch (Exception ex)
            {
                log.ErrorException("Error open port", ex);
            }

            Stopwatch sw = new Stopwatch();
            string time = "0";

            while (true)
            {
                int read = ThinkGear.TG_ReadPackets(con, 1);

                if (read == 1 && ThinkGear.TG_GetValueStatus(con, ThinkGear.DATA_ATTENTION) != 0)
                {
                    float atten = ThinkGear.TG_GetValue(con, ThinkGear.DATA_ATTENTION);
                    p.StoreData(atten);

                    if (!sw.IsRunning)
                    {
                        sw.Start();
                    }
                    else
                    {
                        sw.Stop();
                        TimeSpan ts = sw.Elapsed;
                        time = ts.Minutes.ToString() + ":" + ts.Seconds.ToString();
                        sw.Start();
                    }

                    label1.Invoke(new MethodInvoker(delegate
                    {
                        label1.Text = time;
                    }));
                    gp.AddPoints(atten);
                }
            }
        }

        private void cn_button_Click(object sender, EventArgs e)
        {
            if (!th1.IsAlive)
            {
                th1.Start();
            }
        }

        private void dc_button_Click(object sender, EventArgs e)
        {
            CloseCon();
            p.SaveData();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = form_text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CloseCon();

            this.Close();
        }

        private void CloseCon()
        {
            try
            {
                if (th1.IsAlive)
                {
                    th1.Abort();
                    ThinkGear.TG_FreeConnection(con);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (th1.IsAlive)
                {
                    th1.Abort();
                    ThinkGear.TG_FreeConnection(con);
                }
            }
        }

        private void comPort_cb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
