﻿namespace MindWave
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.audio_btn = new System.Windows.Forms.Button();
            this.video_btn = new System.Windows.Forms.Button();
            this.kinect_btn = new System.Windows.Forms.Button();
            this.input_gb = new System.Windows.Forms.GroupBox();
            this.new_bnt = new System.Windows.Forms.Button();
            this.age_tb = new System.Windows.Forms.MaskedTextBox();
            this.type_cb = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.input_btn = new System.Windows.Forms.Button();
            this.class_tb = new System.Windows.Forms.TextBox();
            this.fullName_tb = new System.Windows.Forms.TextBox();
            this.btns_gb = new System.Windows.Forms.GroupBox();
            this.vi_stat_pan = new System.Windows.Forms.Panel();
            this.kin_stat_pan = new System.Windows.Forms.Panel();
            this.au_stat_pan = new System.Windows.Forms.Panel();
            this.stat_btn = new System.Windows.Forms.Button();
            this.load_gb = new System.Windows.Forms.GroupBox();
            this.change_btn = new System.Windows.Forms.Button();
            this.change_type_cb = new System.Windows.Forms.ComboBox();
            this.delete_btn = new System.Windows.Forms.Button();
            this.reTest_bt = new System.Windows.Forms.Button();
            this.loadtest_cb = new System.Windows.Forms.ComboBox();
            this.loadfullname_cb = new System.Windows.Forms.ComboBox();
            this.loadgrade_cb = new System.Windows.Forms.ComboBox();
            this.load_btn = new System.Windows.Forms.Button();
            this.ind_timer = new System.Windows.Forms.Timer(this.components);
            this.input_gb.SuspendLayout();
            this.btns_gb.SuspendLayout();
            this.load_gb.SuspendLayout();
            this.SuspendLayout();
            // 
            // audio_btn
            // 
            this.audio_btn.Location = new System.Drawing.Point(6, 56);
            this.audio_btn.Name = "audio_btn";
            this.audio_btn.Size = new System.Drawing.Size(184, 23);
            this.audio_btn.TabIndex = 0;
            this.audio_btn.Text = "Аудиальный канал";
            this.audio_btn.UseVisualStyleBackColor = true;
            this.audio_btn.Click += new System.EventHandler(this.audio_btn_Click);
            // 
            // video_btn
            // 
            this.video_btn.Location = new System.Drawing.Point(6, 85);
            this.video_btn.Name = "video_btn";
            this.video_btn.Size = new System.Drawing.Size(184, 23);
            this.video_btn.TabIndex = 1;
            this.video_btn.Text = "Визуальный канал";
            this.video_btn.UseVisualStyleBackColor = true;
            this.video_btn.Click += new System.EventHandler(this.video_btn_Click);
            // 
            // kinect_btn
            // 
            this.kinect_btn.Location = new System.Drawing.Point(6, 114);
            this.kinect_btn.Name = "kinect_btn";
            this.kinect_btn.Size = new System.Drawing.Size(184, 23);
            this.kinect_btn.TabIndex = 2;
            this.kinect_btn.Text = "Кинестетический канал";
            this.kinect_btn.UseVisualStyleBackColor = true;
            this.kinect_btn.Click += new System.EventHandler(this.kinect_btn_Click);
            // 
            // input_gb
            // 
            this.input_gb.Controls.Add(this.new_bnt);
            this.input_gb.Controls.Add(this.age_tb);
            this.input_gb.Controls.Add(this.type_cb);
            this.input_gb.Controls.Add(this.label4);
            this.input_gb.Controls.Add(this.label3);
            this.input_gb.Controls.Add(this.label2);
            this.input_gb.Controls.Add(this.label1);
            this.input_gb.Controls.Add(this.input_btn);
            this.input_gb.Controls.Add(this.class_tb);
            this.input_gb.Controls.Add(this.fullName_tb);
            this.input_gb.Location = new System.Drawing.Point(12, 12);
            this.input_gb.Name = "input_gb";
            this.input_gb.Size = new System.Drawing.Size(608, 184);
            this.input_gb.TabIndex = 3;
            this.input_gb.TabStop = false;
            this.input_gb.Text = "Регистрационные данные";
            // 
            // new_bnt
            // 
            this.new_bnt.Location = new System.Drawing.Point(377, 144);
            this.new_bnt.Name = "new_bnt";
            this.new_bnt.Size = new System.Drawing.Size(116, 23);
            this.new_bnt.TabIndex = 9;
            this.new_bnt.Text = "Новый ученик";
            this.new_bnt.UseVisualStyleBackColor = true;
            this.new_bnt.Click += new System.EventHandler(this.new_bnt_Click);
            // 
            // age_tb
            // 
            this.age_tb.Location = new System.Drawing.Point(7, 45);
            this.age_tb.Name = "age_tb";
            this.age_tb.Size = new System.Drawing.Size(121, 20);
            this.age_tb.TabIndex = 1;
            this.age_tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.age_tb_KeyPress);
            // 
            // type_cb
            // 
            this.type_cb.FormattingEnabled = true;
            this.type_cb.Location = new System.Drawing.Point(6, 70);
            this.type_cb.Name = "type_cb";
            this.type_cb.Size = new System.Drawing.Size(362, 21);
            this.type_cb.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(134, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Возраст";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(374, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Предполагаемый тип восприятия";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(374, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Класс";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(374, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "ФИО";
            // 
            // input_btn
            // 
            this.input_btn.Location = new System.Drawing.Point(377, 114);
            this.input_btn.Name = "input_btn";
            this.input_btn.Size = new System.Drawing.Size(117, 23);
            this.input_btn.TabIndex = 4;
            this.input_btn.Text = "Зарегистрировать";
            this.input_btn.UseVisualStyleBackColor = true;
            this.input_btn.Click += new System.EventHandler(this.input_btn_Click);
            // 
            // class_tb
            // 
            this.class_tb.Location = new System.Drawing.Point(240, 45);
            this.class_tb.Name = "class_tb";
            this.class_tb.Size = new System.Drawing.Size(128, 20);
            this.class_tb.TabIndex = 2;
            // 
            // fullName_tb
            // 
            this.fullName_tb.Location = new System.Drawing.Point(6, 19);
            this.fullName_tb.Name = "fullName_tb";
            this.fullName_tb.Size = new System.Drawing.Size(362, 20);
            this.fullName_tb.TabIndex = 0;
            // 
            // btns_gb
            // 
            this.btns_gb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btns_gb.Controls.Add(this.vi_stat_pan);
            this.btns_gb.Controls.Add(this.kin_stat_pan);
            this.btns_gb.Controls.Add(this.au_stat_pan);
            this.btns_gb.Controls.Add(this.stat_btn);
            this.btns_gb.Controls.Add(this.audio_btn);
            this.btns_gb.Controls.Add(this.video_btn);
            this.btns_gb.Controls.Add(this.kinect_btn);
            this.btns_gb.Location = new System.Drawing.Point(626, 12);
            this.btns_gb.Name = "btns_gb";
            this.btns_gb.Size = new System.Drawing.Size(231, 374);
            this.btns_gb.TabIndex = 4;
            this.btns_gb.TabStop = false;
            // 
            // vi_stat_pan
            // 
            this.vi_stat_pan.BackColor = System.Drawing.SystemColors.Control;
            this.vi_stat_pan.Location = new System.Drawing.Point(196, 85);
            this.vi_stat_pan.Name = "vi_stat_pan";
            this.vi_stat_pan.Size = new System.Drawing.Size(21, 23);
            this.vi_stat_pan.TabIndex = 5;
            // 
            // kin_stat_pan
            // 
            this.kin_stat_pan.BackColor = System.Drawing.SystemColors.Control;
            this.kin_stat_pan.Location = new System.Drawing.Point(196, 114);
            this.kin_stat_pan.Name = "kin_stat_pan";
            this.kin_stat_pan.Size = new System.Drawing.Size(21, 23);
            this.kin_stat_pan.TabIndex = 5;
            // 
            // au_stat_pan
            // 
            this.au_stat_pan.BackColor = System.Drawing.SystemColors.Control;
            this.au_stat_pan.Location = new System.Drawing.Point(196, 56);
            this.au_stat_pan.Name = "au_stat_pan";
            this.au_stat_pan.Size = new System.Drawing.Size(21, 23);
            this.au_stat_pan.TabIndex = 4;
            // 
            // stat_btn
            // 
            this.stat_btn.Location = new System.Drawing.Point(6, 194);
            this.stat_btn.Name = "stat_btn";
            this.stat_btn.Size = new System.Drawing.Size(219, 23);
            this.stat_btn.TabIndex = 3;
            this.stat_btn.Text = "Сводная статистика";
            this.stat_btn.UseVisualStyleBackColor = true;
            this.stat_btn.Click += new System.EventHandler(this.stat_btn_Click);
            // 
            // load_gb
            // 
            this.load_gb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.load_gb.Controls.Add(this.change_btn);
            this.load_gb.Controls.Add(this.change_type_cb);
            this.load_gb.Controls.Add(this.delete_btn);
            this.load_gb.Controls.Add(this.reTest_bt);
            this.load_gb.Controls.Add(this.loadtest_cb);
            this.load_gb.Controls.Add(this.loadfullname_cb);
            this.load_gb.Controls.Add(this.loadgrade_cb);
            this.load_gb.Controls.Add(this.load_btn);
            this.load_gb.Location = new System.Drawing.Point(12, 202);
            this.load_gb.Name = "load_gb";
            this.load_gb.Size = new System.Drawing.Size(608, 184);
            this.load_gb.TabIndex = 5;
            this.load_gb.TabStop = false;
            this.load_gb.Text = "Загрузить данные";
            // 
            // change_btn
            // 
            this.change_btn.Enabled = false;
            this.change_btn.Location = new System.Drawing.Point(377, 99);
            this.change_btn.Name = "change_btn";
            this.change_btn.Size = new System.Drawing.Size(116, 23);
            this.change_btn.TabIndex = 16;
            this.change_btn.Text = "Изменить";
            this.change_btn.UseVisualStyleBackColor = true;
            this.change_btn.Click += new System.EventHandler(this.change_btn_Click);
            // 
            // change_type_cb
            // 
            this.change_type_cb.Enabled = false;
            this.change_type_cb.FormattingEnabled = true;
            this.change_type_cb.Location = new System.Drawing.Point(7, 101);
            this.change_type_cb.Name = "change_type_cb";
            this.change_type_cb.Size = new System.Drawing.Size(361, 21);
            this.change_type_cb.TabIndex = 15;
            this.change_type_cb.Text = "Выберите тип";
            // 
            // delete_btn
            // 
            this.delete_btn.Location = new System.Drawing.Point(377, 73);
            this.delete_btn.Name = "delete_btn";
            this.delete_btn.Size = new System.Drawing.Size(117, 23);
            this.delete_btn.TabIndex = 14;
            this.delete_btn.Text = "Удалить";
            this.delete_btn.UseVisualStyleBackColor = true;
            this.delete_btn.Click += new System.EventHandler(this.delete_btn_Click);
            // 
            // reTest_bt
            // 
            this.reTest_bt.Location = new System.Drawing.Point(377, 46);
            this.reTest_bt.Name = "reTest_bt";
            this.reTest_bt.Size = new System.Drawing.Size(117, 23);
            this.reTest_bt.TabIndex = 13;
            this.reTest_bt.Text = "Перетестировать";
            this.reTest_bt.UseVisualStyleBackColor = true;
            this.reTest_bt.Click += new System.EventHandler(this.reTest_bt_Click);
            // 
            // loadtest_cb
            // 
            this.loadtest_cb.FormattingEnabled = true;
            this.loadtest_cb.Location = new System.Drawing.Point(7, 73);
            this.loadtest_cb.Name = "loadtest_cb";
            this.loadtest_cb.Size = new System.Drawing.Size(361, 21);
            this.loadtest_cb.TabIndex = 12;
            this.loadtest_cb.Text = "Выберите тест";
            // 
            // loadfullname_cb
            // 
            this.loadfullname_cb.FormattingEnabled = true;
            this.loadfullname_cb.Location = new System.Drawing.Point(7, 46);
            this.loadfullname_cb.Name = "loadfullname_cb";
            this.loadfullname_cb.Size = new System.Drawing.Size(361, 21);
            this.loadfullname_cb.TabIndex = 11;
            this.loadfullname_cb.Text = "Выберите ученика";
            this.loadfullname_cb.SelectedIndexChanged += new System.EventHandler(this.loadfullname_cb_SelectedIndexChanged);
            // 
            // loadgrade_cb
            // 
            this.loadgrade_cb.FormattingEnabled = true;
            this.loadgrade_cb.Location = new System.Drawing.Point(7, 19);
            this.loadgrade_cb.Name = "loadgrade_cb";
            this.loadgrade_cb.Size = new System.Drawing.Size(361, 21);
            this.loadgrade_cb.TabIndex = 10;
            this.loadgrade_cb.Text = "Выберите класс";
            this.loadgrade_cb.SelectedIndexChanged += new System.EventHandler(this.loadgrade_cb_SelectedIndexChanged);
            // 
            // load_btn
            // 
            this.load_btn.Location = new System.Drawing.Point(377, 19);
            this.load_btn.Name = "load_btn";
            this.load_btn.Size = new System.Drawing.Size(117, 23);
            this.load_btn.TabIndex = 0;
            this.load_btn.Text = "Загрузить";
            this.load_btn.UseVisualStyleBackColor = true;
            this.load_btn.Click += new System.EventHandler(this.load_btn_Click);
            // 
            // ind_timer
            // 
            this.ind_timer.Interval = 10;
            this.ind_timer.Tick += new System.EventHandler(this.ind_timer_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 398);
            this.Controls.Add(this.load_gb);
            this.Controls.Add(this.btns_gb);
            this.Controls.Add(this.input_gb);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(885, 436);
            this.MinimumSize = new System.Drawing.Size(885, 436);
            this.Name = "MainForm";
            this.Text = "Форма регистрации";
            this.input_gb.ResumeLayout(false);
            this.input_gb.PerformLayout();
            this.btns_gb.ResumeLayout(false);
            this.load_gb.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button audio_btn;
        private System.Windows.Forms.Button video_btn;
        private System.Windows.Forms.Button kinect_btn;
        private System.Windows.Forms.GroupBox input_gb;
        private System.Windows.Forms.Button input_btn;
        private System.Windows.Forms.TextBox class_tb;
        private System.Windows.Forms.TextBox fullName_tb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox btns_gb;
        private System.Windows.Forms.GroupBox load_gb;
        private System.Windows.Forms.Button load_btn;
        private System.Windows.Forms.ComboBox loadgrade_cb;
        private System.Windows.Forms.Button stat_btn;
        private System.Windows.Forms.ComboBox type_cb;
        private System.Windows.Forms.ComboBox loadtest_cb;
        private System.Windows.Forms.ComboBox loadfullname_cb;
        private System.Windows.Forms.Button reTest_bt;
        private System.Windows.Forms.Button delete_btn;
        private System.Windows.Forms.MaskedTextBox age_tb;
        private System.Windows.Forms.Panel vi_stat_pan;
        private System.Windows.Forms.Panel kin_stat_pan;
        private System.Windows.Forms.Panel au_stat_pan;
        private System.Windows.Forms.Timer ind_timer;
        private System.Windows.Forms.Button change_btn;
        private System.Windows.Forms.ComboBox change_type_cb;
        private System.Windows.Forms.Button new_bnt;
    }
}

