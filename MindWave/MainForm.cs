﻿using NLog;
using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MindWave
{
    public partial class MainForm : Form
    {
        const string AUDIO = "Аудиальный канал";
        const string VIDEO = "Визуальный канал";
        const string KINECT = "Кинестетический канал";
        const int AU = 0;
        const int VI = 1;
        const int KI = 2;
        GraphForm gf;
        Person p = new Person();
        private static Logger log = LogManager.GetCurrentClassLogger();

        [Conditional("DEBUG")]
        private void TEST()
        {
            string[] testnames = new string[] { "test name 1", "test name 2", "test name 3", "test name 4", "test name 5" };
            int[] testages = new int[] { 1, 2, 4, 5, 6 };
            string[] testgrades = new string[] { "3a", "3a", "4d", "2e", "5f" };
            string[] testtypes = new string[] { "Визуал", "Визуал", "Визуал", "Визуал", "Визуал" };

            for (int i = 0; i < 5; i++)
            {
                AddPerson(testnames[i], testages[i].ToString(), testgrades[i], testtypes[i]);
            }
        }

        public MainForm()
        {
            //TEST();
            InitializeComponent();
            btns_gb.Enabled = false;                

            this.StartPosition = FormStartPosition.CenterScreen;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;

            string subPath = @"\Data\";
            bool IsExists = System.IO.Directory.Exists(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + subPath);
            if (!IsExists)
            {
                try
                {
                    System.IO.Directory.CreateDirectory(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + subPath);
                }
                catch (Exception ex)
                {
                    log.ErrorException("Create dir error", ex);
                }
            }

            type_cb.Items.AddRange(new string[] { "Неизвестен", "Аудиал", "Визуал", "Кинестет" });
            loadgrade_cb.Items.AddRange(IOdata.GetGrade());
        }

        private void audio_btn_Click(object sender, EventArgs e)
        {
            if (p.IsAudio)
            {
                StatForm sf = new StatForm(p.Audio, p.FullName, p.Test, p.Grade, p.Age, p.Type);
                sf.Show("Статистика: " + AUDIO);
            }
            else
            {
                MakeTest(AUDIO, AU, p);
            }
        }

        private void video_btn_Click(object sender, EventArgs e)
        {
            if (p.IsVideo)
            {
                StatForm sf = new StatForm(p.Video, p.FullName, p.Test, p.Grade, p.Age, p.Type);
                sf.Show("Статистика: " + VIDEO);
            }
            else
            {
                MakeTest(VIDEO, VI, p);
            }
        }

        private void kinect_btn_Click(object sender, EventArgs e)
        {
            if (p.IsKinect)
            {
                StatForm sf = new StatForm(p.Kinect, p.FullName, p.Test, p.Grade, p.Age, p.Type);
                sf.Show("Статистика: " + KINECT);
            }
            else
            {
                MakeTest(KINECT, KI, p);
            }
        }

        private void MakeTest(string channel, int ch, Person p)
        {
            gf = new GraphForm(channel, ch, p);
            gf.Show();
        }

        private void AddPerson(string fullName, string age, string grade, string type)
        {
            p.FullName = fullName;
            p.Age = age;
            p.Grade = grade;
            p.Type = type;

            try
            {
                p.SavePerson();
            }
            catch (Exception e)
            {
                log.ErrorException("AddPerson error", e);
            }
        }

        private void input_btn_Click(object sender, EventArgs e)
        {
            if (fullName_tb.Text.Trim().Length != 0 
                && class_tb.Text.Trim().Length != 0
                && type_cb.SelectedIndex != -1
                && age_tb.Text.Trim().Length != 0)
            {
                foreach (Control c in input_gb.Controls)
                {
                    if (c != new_bnt)
                    {
                        c.Enabled = false;
                    }
                }

                btns_gb.Enabled = true;

                p.Clear();

                AddPerson(fullName_tb.Text,
                            age_tb.Text,
                            class_tb.Text,
                            type_cb.SelectedItem.ToString());

                au_stat_pan.BackColor = Color.Lime;
                vi_stat_pan.BackColor = Color.Lime;
                kin_stat_pan.BackColor = Color.Lime;

                StartTimer();
            }
        }

        private void StartTimer()
        {
            if (!ind_timer.Enabled)
            {
                ind_timer.Start();
            }
        }

        private void load_btn_Click(object sender, EventArgs e)
        {
            if (CheckInput())
            {

                string path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + "\\Data\\";
                string file = loadgrade_cb.SelectedItem.ToString() + "-" +
                                loadfullname_cb.SelectedItem.ToString() + "-" +
                                loadtest_cb.SelectedItem.ToString().Replace(".", "").Replace(":", "");
                string ext = ".tst";

                try
                {
                    p.Load(path, file, ext);
                }
                catch (Exception ex)
                {
                    log.ErrorException("Load file " + file + " error", ex);
                }

                fullName_tb.Text = p.FullName;
                age_tb.Text = p.Age;
                class_tb.Text = p.Grade;
                type_cb.SelectedItem = p.Type;

                if (p.Type.Equals("Неизвестен"))
                {
                    change_type_cb.Items.AddRange(new string[] { "Аудиал", "Визуал", "Кинестет" });
                    change_type_cb.Enabled = true;
                    change_btn.Enabled = true;
                }

                foreach (Control c in input_gb.Controls)
                {
                    if (c != new_bnt)
                    {
                        c.Enabled = false;
                    }
                }

                btns_gb.Enabled = true;

                if (p.IsAudio)
                {
                    au_stat_pan.BackColor = Color.Red;
                }
                if (p.IsVideo)
                {
                    vi_stat_pan.BackColor = Color.Red;
                }
                if (p.IsKinect)
                {
                    kin_stat_pan.BackColor = Color.Red;
                }

                StartTimer();
            }
        }

        private void stat_btn_Click(object sender, EventArgs e)
        {
            if (p.IsAudio || p.IsVideo || p.IsKinect)
            {
                StatForm sf = new StatForm(p);
                sf.Show("Сводная статистика");
            }
        }

        private void loadgrade_cb_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadfullname_cb.Items.Clear();
            loadfullname_cb.Items.AddRange(IOdata.GetFullName(loadgrade_cb.SelectedItem.ToString()));
            loadfullname_cb.Text = "Выберите ученика";
            loadfullname_cb.SelectedIndex = -1;
            loadtest_cb.SelectedIndex = -1;
        }

        private void loadfullname_cb_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadtest_cb.Items.Clear();
            loadtest_cb.Items.AddRange(IOdata.GetTestByTime(loadfullname_cb.SelectedItem.ToString()));
            loadtest_cb.Text = "Выберите тест";
            loadtest_cb.SelectedIndex = -1;
        }

        private void reTest_bt_Click(object sender, EventArgs e)
        {
            if (CheckInput())
            {
                fullName_tb.Text = p.FullName;
                age_tb.Text = p.Age;
                class_tb.Text = p.Grade;
                type_cb.SelectedItem = p.Type;

                foreach (Control c in input_gb.Controls)
                {
                    c.Enabled = true;
                }
                btns_gb.Enabled = false;
            }
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            if (CheckInput() && MessageBox.Show("Удалить " + loadfullname_cb.SelectedItem.ToString() + ", "
                                + loadgrade_cb.SelectedItem.ToString() + ", "
                                + loadtest_cb.SelectedItem.ToString() + "?",
                                "Удалить?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                string path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + "\\Data\\";
                string file = loadgrade_cb.SelectedItem.ToString() + "-" +
                                loadfullname_cb.SelectedItem.ToString() + "-" +
                                loadtest_cb.SelectedItem.ToString().Replace(".", "").Replace(":", "");
                string ext = ".tst";
                try
                {
                    File.Delete(path + file + ext);
                }
                catch (Exception ex)
                {
                    log.ErrorException("Delete file error", ex);
                }

                loadgrade_cb.Items.Clear();
                loadfullname_cb.Items.Remove(loadfullname_cb.SelectedItem);
                loadtest_cb.Items.Remove(loadtest_cb.SelectedItem);
                loadgrade_cb.SelectedIndex = -1;
                loadfullname_cb.SelectedIndex = -1;
                loadtest_cb.SelectedIndex = -1;

                loadgrade_cb.Items.AddRange(IOdata.GetGrade());

                loadgrade_cb.Text = "Выберите класс";
                loadfullname_cb.Text = "Выберите ученика";
                loadtest_cb.Text = "Выберите тест";
                change_type_cb.Text = "Выберите тип";
            }
        }

        private void age_tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar != '\b')
            {
                e.Handled = !char.IsNumber(e.KeyChar);
            }
        }

        private void ind_timer_Tick(object sender, EventArgs e)
        {
            if (p.IsAudio)
            {
                au_stat_pan.BackColor = Color.Red;
            }
            else
            {
                au_stat_pan.BackColor = Color.Lime;
            }
            if (p.IsVideo)
            {
                vi_stat_pan.BackColor = Color.Red;
            }
            else
            {
                vi_stat_pan.BackColor = Color.Lime;
            }
            if (p.IsKinect)
            {
                kin_stat_pan.BackColor = Color.Red;
            }
            else
            {
                kin_stat_pan.BackColor = Color.Lime;
            }

            if (p.IsAudio && p.IsVideo && p.IsKinect)
            {
                ind_timer.Stop();
            }
        }

        private void change_btn_Click(object sender, EventArgs e)
        {
            if (change_type_cb.SelectedIndex != -1)
            {
                p.ChangeType(change_type_cb.SelectedItem.ToString());

                change_btn.Enabled = false;
                change_type_cb.Enabled = false;
                type_cb.Text = change_type_cb.SelectedItem.ToString();
            }
        }

        private void new_bnt_Click(object sender, EventArgs e)
        {
            try
            {
                p.Clear();
                p = new Person();
                loadgrade_cb.Items.Clear();
                loadgrade_cb.Items.AddRange(IOdata.GetGrade());
            }
            catch (Exception ex)
            {
                log.ErrorException("Clear person error", ex);
            }

            foreach (Control c in input_gb.Controls)
            {
                c.Enabled = true;
            }
            btns_gb.Enabled = false;

            fullName_tb.Text = "";
            age_tb.Text = "";
            class_tb.Text = "";
            type_cb.SelectedIndex = -1;
        }

        private bool CheckInput()
        {
            return loadgrade_cb.SelectedIndex != -1 && loadfullname_cb.SelectedIndex != -1 && loadtest_cb.SelectedIndex != -1;
        }
    }
}
