﻿namespace MindWave
{
    partial class StatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.stat_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.print_bt = new System.Windows.Forms.Button();
            this.preview_bt = new System.Windows.Forms.Button();
            this.pgStp_bt = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.stat_chart)).BeginInit();
            this.SuspendLayout();
            // 
            // stat_chart
            // 
            this.stat_chart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.stat_chart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.stat_chart.Legends.Add(legend2);
            this.stat_chart.Location = new System.Drawing.Point(12, 12);
            this.stat_chart.Name = "stat_chart";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.stat_chart.Series.Add(series2);
            this.stat_chart.Size = new System.Drawing.Size(764, 374);
            this.stat_chart.TabIndex = 0;
            this.stat_chart.Text = "chart1";
            this.stat_chart.CustomizeLegend += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.CustomizeLegendEventArgs>(this.stat_chart_CustomizeLegend);
            this.stat_chart.MouseClick += new System.Windows.Forms.MouseEventHandler(this.stat_chart_MouseClick);
            // 
            // print_bt
            // 
            this.print_bt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.print_bt.Location = new System.Drawing.Point(782, 42);
            this.print_bt.Name = "print_bt";
            this.print_bt.Size = new System.Drawing.Size(75, 23);
            this.print_bt.TabIndex = 1;
            this.print_bt.Text = "Печать";
            this.print_bt.UseVisualStyleBackColor = true;
            this.print_bt.Click += new System.EventHandler(this.print_bt_Click);
            // 
            // preview_bt
            // 
            this.preview_bt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.preview_bt.Location = new System.Drawing.Point(782, 71);
            this.preview_bt.Name = "preview_bt";
            this.preview_bt.Size = new System.Drawing.Size(75, 23);
            this.preview_bt.TabIndex = 2;
            this.preview_bt.Text = "Просмотр";
            this.preview_bt.UseVisualStyleBackColor = true;
            this.preview_bt.Click += new System.EventHandler(this.preview_bt_Click);
            // 
            // pgStp_bt
            // 
            this.pgStp_bt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pgStp_bt.Location = new System.Drawing.Point(782, 13);
            this.pgStp_bt.Name = "pgStp_bt";
            this.pgStp_bt.Size = new System.Drawing.Size(75, 23);
            this.pgStp_bt.TabIndex = 3;
            this.pgStp_bt.Text = "Настройки";
            this.pgStp_bt.UseVisualStyleBackColor = true;
            this.pgStp_bt.Click += new System.EventHandler(this.pgStp_bt_Click);
            // 
            // StatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 398);
            this.Controls.Add(this.pgStp_bt);
            this.Controls.Add(this.preview_bt);
            this.Controls.Add(this.print_bt);
            this.Controls.Add(this.stat_chart);
            this.MinimumSize = new System.Drawing.Size(885, 436);
            this.Name = "StatForm";
            this.Text = "StatForm";
            this.Load += new System.EventHandler(this.StatForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.stat_chart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart stat_chart;
        private System.Windows.Forms.Button print_bt;
        private System.Windows.Forms.Button preview_bt;
        private System.Windows.Forms.Button pgStp_bt;


    }
}