﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MindWave
{
    static class IOdata
    {
        private static string[] files;
        private static string[] grades;
        private static string[] fullnames;
        private static string[] tests;
        private static bool loadflag = false;
        private static string subPath = @"\Data\";

        private static Logger log = LogManager.GetCurrentClassLogger();

        public static void Save(string file, StringBuilder sb)
        {
            try
            {
                string path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + subPath + file;
                System.IO.File.AppendAllText(path, sb.ToString());
            }
            catch (Exception ex)
            {
                log.ErrorException("Save to file error", ex);
            }
        }

        public static void Edit(string file, StringBuilder sb)
        {
            try
            {
                string path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + subPath + file;
                System.IO.File.WriteAllText(path, sb.ToString());
            }
            catch (Exception ex)
            {
                log.ErrorException("Edit to file error", ex);
            }
        }

        public static List<string> LoadData(string path)
        {
            string line;
            List<string> lines = new List<string>();
            StreamReader file = new StreamReader(@path);
            try
            {
                while ((line = file.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }
            catch (Exception ex)
            {
                log.ErrorException("LoadData to file error", ex);
            }
            finally
            {
                file.Close();
            }
            return lines;
        }

        private static void GetFileNames()
        {
            string folder = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + subPath;
            string ext = "*.tst";
            string[] paths = Directory.GetFiles(folder, ext);
            files = new string[paths.Length];
            grades = new string[paths.Length];
            fullnames = new string[paths.Length];
            tests = new string[paths.Length];
            for (int i = 0; i < files.Length; i++)
            {
                files[i] = Regex.Match(paths[i], "(?<=Data\\\\).*(?=.tst)").ToString();
                string[] parts = files[i].Split('-');
                grades[i] = parts[0];
                fullnames[i] = parts[1];
                DateTime tmp = DateTime.ParseExact(parts[2], "ddMMyyyy HHmmss",
                    System.Globalization.CultureInfo.InvariantCulture);
                tests[i] = tmp.ToString();
            }
            loadflag = true;
        }

        public static string[] GetGrade()
        {
            GetFileNames();
            return grades.Distinct().ToArray();
        }

        public static string[] GetFullName(string grade)
        {
            if (!loadflag)
            {
            GetFileNames();
            }
            var res = Array.FindAll(files, delegate(string s) { return s.Contains(grade); }); //BUG!
            fullnames = new string[res.Length];
            for (int i = 0; i < res.Length; i++)
            {
                string[] parts = res[i].Split('-');
                fullnames[i] = parts[1];
                DateTime tmp = DateTime.ParseExact(parts[2], "ddMMyyyy HHmmss",
                    System.Globalization.CultureInfo.InvariantCulture);
                tests[i] = tmp.ToString();
            }
            return fullnames.Distinct().ToArray();
        }

        public static string[] GetTestByTime(string fullname)
        {
            if (!loadflag)
            {
            GetFileNames();
            }
            var res = Array.FindAll(files, delegate(string s) { return s.Contains(fullname); });
            tests = new string[res.Length];
            for (int i = 0; i < res.Length; i++)
            {
                string[] parts = res[i].Split('-');
                DateTime tmp = DateTime.ParseExact(parts[2], "ddMMyyyy HHmmss",
                    System.Globalization.CultureInfo.InvariantCulture);
                tests[i] = tmp.Date.ToString("dd.MM.yyyy") + " " + tmp.TimeOfDay.ToString();
            }
            return tests.Distinct().ToArray();
        }
    }
}
