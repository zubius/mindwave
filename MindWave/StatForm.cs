﻿using NLog;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MindWave
{
    public partial class StatForm : Form
    {
        private string _formText = "";
        private string[] _parts = { "1 - 25", "25 - 50", "50 - 75", "75 - 100" };
        private Color[] _colors = { Color.SkyBlue, Color.Gold, Color.OrangeRed, Color.LimeGreen };

        private static Logger log = LogManager.GetCurrentClassLogger();

        public StatForm(List<int> data, string fullname, string test, 
                            string grade, string age, string type)
        {
            InitializeComponent();
            int[] datas = Sort(data);
            stat_chart.Series.Clear();
            stat_chart.ChartAreas.Clear();

            stat_chart.Palette = ChartColorPalette.None;
            stat_chart.PaletteCustomColors = _colors;

            stat_chart.Series.Add("Series");
            stat_chart.ChartAreas.Add("ChartArea");
            stat_chart.Series[0].ChartArea = "ChartArea";
            stat_chart.Series[0].ChartType = SeriesChartType.Pie;
            stat_chart.Series[0].Points.DataBindXY(_parts, datas);
            for (int i = 0; i < datas.Length; i++)
            {              
                stat_chart.Series[0].Points[i].ToolTip = "#VALY";
            }
            Title t = new Title(fullname + ", " + grade + ", " + type + ", " + test, Docking.Top);
            stat_chart.Titles.Add(t);

            stat_chart.Series[0]["PieLabelStyle"] = "Outside";
            stat_chart.ChartAreas[0].Area3DStyle.Enable3D = true;
        }

        public StatForm(Person p)
        {
            InitializeComponent();
            int chnumber = 3;
            int[][] datas = new int[chnumber][];
            datas[0] = Sort(p.Audio);
            datas[1] = Sort(p.Video);
            datas[2] = Sort(p.Kinect);

            stat_chart.Series.Clear();
            stat_chart.ChartAreas.Clear();

            stat_chart.Palette = ChartColorPalette.None;
            stat_chart.PaletteCustomColors = _colors;

            for (int i = 0; i < chnumber; i++)
            {
                stat_chart.Series.Add("Series" + i.ToString());
                stat_chart.ChartAreas.Add("ChartArea" + i.ToString());
                stat_chart.ChartAreas[i].Area3DStyle.Enable3D = true;
                stat_chart.Series[i].ChartArea = "ChartArea" + i.ToString();
                stat_chart.Series[i].ChartType = SeriesChartType.Pie;
                //stat_chart.Series[i].Points.DataBindXY(_parts, datas[i]);
                
                for (int j = 0; j < datas[i].Length; j++)
                {
                    stat_chart.Series[i].Points.AddY(datas[i][j]);
                    stat_chart.Series[i].Points[j].ToolTip = "#VALY";
                }
            }

            Title t = new Title(p.FullName + ", " + p.Grade + ", " + p.Type + ", " + p.Test, Docking.Top);
            Title at = new Title("Аудиальный канал", Docking.Top);
            Title vt = new Title("Визуальный канал", Docking.Top);
            Title kt = new Title("Кинестетический канал", Docking.Top);
            at.DockedToChartArea = stat_chart.ChartAreas[0].Name;
            vt.DockedToChartArea = stat_chart.ChartAreas[1].Name;
            kt.DockedToChartArea = stat_chart.ChartAreas[2].Name;
            at.IsDockedInsideChartArea = false;
            vt.IsDockedInsideChartArea = false;
            kt.IsDockedInsideChartArea = false;
            stat_chart.Titles.Add(t);
            stat_chart.Titles.Add(at);
            stat_chart.Titles.Add(vt);
            stat_chart.Titles.Add(kt);
            /*stat_chart.Series[0]["PieLabelStyle"] = "Inside";
            stat_chart.Series[1]["PieLabelStyle"] = "Outside";
            stat_chart.Series[2]["PieLabelStyle"] = "Inside";*/

            stat_chart.ChartAreas[0].Position.X = 0;
            stat_chart.ChartAreas[0].Position.Y = 33;
            stat_chart.ChartAreas[0].Position.Width = 33;
            stat_chart.ChartAreas[0].Position.Height = 33;

            stat_chart.ChartAreas[1].Position.X = 33;
            stat_chart.ChartAreas[1].Position.Y = 33;
            stat_chart.ChartAreas[1].Position.Width = 33;
            stat_chart.ChartAreas[1].Position.Height = 33;

            stat_chart.ChartAreas[2].Position.X = 66;
            stat_chart.ChartAreas[2].Position.Y = 33;
            stat_chart.ChartAreas[2].Position.Width = 33;
            stat_chart.ChartAreas[2].Position.Height = 33;

            stat_chart.Series[1].IsVisibleInLegend = false;
            stat_chart.Series[2].IsVisibleInLegend = false;
        }

        private int[] Sort(List<int> data)
        {
            int[] count = new int[4];
            foreach (int d in data)
            {
                if (d >= 0 && d <= 25)
                    count[0]++;
                else if (d >= 26 && d <= 50)
                    count[1]++;
                else if (d >= 51 && d <= 75)
                    count[2]++;
                else
                    count[3]++;
            }
            return count;
        }

        private void stat_chart_CustomizeLegend(object sender, CustomizeLegendEventArgs e)
        {
            for (int i = 0; i < _parts.Length; i++)
                e.LegendItems[i].Cells[1].Text = _parts[i];
        }

        private void StatForm_Load(object sender, EventArgs e)
        {
            this.Text = _formText;
        }

        public void Show(string formText)
        {
            this._formText = formText;
            base.Show();
        }

        private void stat_chart_MouseClick(object sender, MouseEventArgs e)
        {
            var pos = e.Location;
            var results = stat_chart.HitTest(pos.X, pos.Y, false, ChartElementType.DataPoint);

            foreach (var res in results)
            {
                if (res.ChartElementType == ChartElementType.DataPoint)
                {
                    if (res.Series.Points[res.PointIndex]["Exploded"] == "true")
                        res.Series.Points[res.PointIndex]["Exploded"] = "false";
                    else
                        res.Series.Points[res.PointIndex]["Exploded"] = "true";

                    foreach (Series serie in stat_chart.Series)
                        foreach (DataPoint point in serie.Points)
                        {
                            if (point != res.Series.Points[res.PointIndex])
                                point["Exploded"] = "false";
                        }
                }
            }
        }

        private void pgStp_bt_Click(object sender, EventArgs e)
        {
            stat_chart.Printing.PageSetup();
        }

        private void preview_bt_Click(object sender, EventArgs e)
        {
            stat_chart.Printing.PrintPreview();
        }

        private void print_bt_Click(object sender, EventArgs e)
        {
            stat_chart.Printing.Print(true);
        }
    }
}
