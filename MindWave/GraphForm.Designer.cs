﻿namespace MindWave
{
    partial class GraphForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cn_button = new System.Windows.Forms.Button();
            this.dc_button = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.comPort_cb = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(779, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // cn_button
            // 
            this.cn_button.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cn_button.Location = new System.Drawing.Point(782, 118);
            this.cn_button.Name = "cn_button";
            this.cn_button.Size = new System.Drawing.Size(75, 23);
            this.cn_button.TabIndex = 1;
            this.cn_button.Text = "Начать";
            this.cn_button.UseVisualStyleBackColor = true;
            this.cn_button.Click += new System.EventHandler(this.cn_button_Click);
            // 
            // dc_button
            // 
            this.dc_button.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.dc_button.Location = new System.Drawing.Point(782, 183);
            this.dc_button.Name = "dc_button";
            this.dc_button.Size = new System.Drawing.Size(75, 23);
            this.dc_button.TabIndex = 2;
            this.dc_button.Text = "Сохранить";
            this.dc_button.UseVisualStyleBackColor = true;
            this.dc_button.Click += new System.EventHandler(this.dc_button_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(782, 251);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Отменить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comPort_cb
            // 
            this.comPort_cb.FormattingEnabled = true;
            this.comPort_cb.Location = new System.Drawing.Point(782, 83);
            this.comPort_cb.Name = "comPort_cb";
            this.comPort_cb.Size = new System.Drawing.Size(75, 21);
            this.comPort_cb.TabIndex = 4;
            this.comPort_cb.SelectedIndexChanged += new System.EventHandler(this.comPort_cb_SelectedIndexChanged);
            // 
            // GraphForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 398);
            this.Controls.Add(this.comPort_cb);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dc_button);
            this.Controls.Add(this.cn_button);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(885, 436);
            this.Name = "GraphForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cn_button;
        private System.Windows.Forms.Button dc_button;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comPort_cb;



    }
}

